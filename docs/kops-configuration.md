

# Cluster Kubernetes na AWS

> O [Kubernetes](https://www.redhat.com/pt-br/topics/containers/what-is-kubernetes) é uma plataforma de orquestração de containers através de um cluster, que possibilita alta disponibilidade da aplicação, isolamento e fácil gerenciamento.



## Requisitos:

#### [AWScli](https://docs.aws.amazon.com/pt_br/cli/latest/userguide/installing.html)

```shell
 pip install awscli boto3
 aws configure
```

Será necessário gerar as chaves de acesso com permissão de administrador no [IAM](https://docs.aws.amazon.com/pt_br/general/latest/gr/managing-aws-access-keys.html).



#### [Kops](https://github.com/kubernetes/kops)

```shell
curl -LO https://github.com/kubernetes/kops/releases/download/$(curl -s https://api.github.com/repos/kubernetes/kops/releases/latest | grep tag_name | cut -d '"' -f 4)/kops-linux-amd64
chmod +x kops-linux-amd64
sudo mv kops-linux-amd64 /usr/bin/kops
```

 

#### [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl)

```shell
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/bin/kubectl
bash echo "source <(kubectl completion bash)" >> ~/.bashrc
```

 

#### [Helm](https://github.com/kubernetes/helm)

```shell
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh
```



#### [Subdomínio](https://docs.aws.amazon.com/pt_br/Route53/latest/DeveloperGuide/CreatingNewSubdomain.html)

```shell
k8s-dev.domain.com
```

O kubernetes irá gerenciar o subdomínio criando automaticamente as entradas e apontamentos para as APIs do cluster.



#### [Bucket S3](https://docs.aws.amazon.com/pt_br/AmazonS3/latest/user-guide/create-bucket.html)

```shell
k8s-bucket
```

O kubernetes irá gravar as configurações nesse bucket.



#### [Chave SSH](https://www.ssh.com/ssh/keygen/)

```shell
ssh-keygen -b 4096 -f id_rsa
```

Essa chave será usada para acessar as máquinas que serão criadas pelo Kops.

 

## Criando a Infra com o Kops

>  O Kops é uma ferramenta para auxiliar a subida do cluster Kubernetes na AWS, basicamente ele se encarrega de criar toda a estrutura básica para que o cluster funcione perfeitamente.

Exemplo de configuração do Kops:

```shell
/sbin/kops create cluster \
--name=k8s-dev.domain.com \
--state=s3://k8s-bucket \
--dns-zone=k8s-dev.domain.com \
--vpc=vpc-4c466e37 \
--topology=private \
--bastion=false \
--kubernetes-version=1.9.10 \
--master-count=1 \
--master-size=t2.medium \
--master-zones=us-east-1a \
--networking=calico \
--node-count=2 \
--node-size=t2.medium \
--node-volume-size=160 \
--zones=us-east-1a,us-east-1c \
--subnets subnet-ce9b5ba9,subnet-a2c645e8 \
--utility-subnets subnet-8cad6deb,subnet-63c94a29 \
--authorization=RBAC \
--ssh-public-key="id_rsa.pub"
```

Com esse comando acima ele irá somente gerar as configurações do cluster e salvar no bucket do S3.

Aplicar as configurações geradas e subir o cluster:

```shell
kops update cluster k8s-dev.domain.com --yes --state s3://k8s-bucket
```


Caso precise editar algo no cluster:

```shell
kops edit cluster --name=k8s-dev.domain.com --state s3://k8s-bucket

kops edit ig --name=k8s-dev.domain.com nodes --state s3://k8s-bucket
kops edit ig --name=k8s-dev.domain.com master-us-east-1c  --state s3://k8s-bucket

kops update cluster k8s-dev.domain.com --yes --state s3://k8s-bucket
kops rolling-update cluster k8s-dev.domain.com --yes --state s3://k8s-bucket
```


## Gerenciando o cluster com o Kubectl

> Kubectl é um utilitário de linha de comando que se conecta ao servidor API, com ele podemos criar pods, deployments, serviços, etc.



### Comandos básicos

Exibir a lista de comandos:

```shell
kubectl help
```

Visualizar configuração:

```shell
kubectl config view
```

Aplicar ou criar uma configuração:

```shell
kubectl apply -f nomedoarquivo.yml
```

Retorna a lista de namespaces:

```shell
kubectl get namespaces
```

Retorna a lista de pods de um namespace específico:

```shell
kubectl get pods -n kube-system
```

Retorna a lista de pods de todos os namespaces:

```shell
kubectl get pods --all-namespaces
```

Exibir o status de um pod:

```shell
kubectl describe pod podnamexxx -n namespacexxx
```

Acessar um pod:

```shell
kubectl exec -it podnamexxx /bin/bash -n namespacexxx
```

Ver logs de um pod:

```shell
kubectl logs podnamexxx -n namespacexxx
```
