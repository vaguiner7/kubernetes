## Monitoramento - Prometheus e Grafana

[Installation Guide - Link](https://github.com/coreos/prometheus-operator/tree/master/helm)

### Criar namespace monitoring
```shell
kubectl create namespace monitoring
```

### Adicionar repositórios
```shell
helm repo add coreos https://s3-eu-west-1.amazonaws.com/coreos-charts/stable/
```

### Instalar Prometheus e Grafana
```shell
helm install coreos/prometheus-operator --name prometheus-operator  --namespace monitoring

helm install coreos/kube-prometheus --name kube-prometheus --set global.rbacEnable=true,grafana.adminUser=admin,grafana.adminPassword=STRONGPASSWORDHERE,grafana.auth.anonymous.enabled=false --namespace monitoring
```

### Acessar página web do Prometheus:
```shell
kubectl port-forward -n monitoring prometheus-kube-prometheus-0 9090
```

### Acessar página web do AlertManager do Prometheus:
```shell
kubectl port-forward -n monitoring alertmanager-kube-prometheus-0 9093
```

### Acessar página web do Grafana:
```shell
kubectl port-forward $(kubectl get  pods --selector=app=kube-prometheus-grafana -n  monitoring --output=jsonpath="{.items..metadata.name}") -n monitoring  3000
```

Se necessário, LoadBalancer para acessar o grafana externamente:

```yaml
cat <<EOF | kubectl create -f -
---
apiVersion: v1
kind: Service
metadata:
  name: grafana-lb
  namespace: monitoring
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-backend-protocol: http
spec:
  selector:
    app: kube-prometheus-grafana
  ports:
  - port: 80
    name: http
    protocol: TCP
    targetPort: 3000
  type: LoadBalancer
  
EOF
```
