# Cluster Kubernetes na AWS

#### - [Kops e Kubectl - Criando e gerenciando o cluster na AWS](docs/kops-configuration.md)

#### - [Ingress Nginx - Controlador de ingresso](docs/ingress-nginx.md)

#### - [Helm Tiller - Gerenciador de pacotes](docs/helm-tiller.md)

#### - [Logs com FluentD - Envio de logs para o ElasticSearch](docs/logs-fluentd.md)

#### - [Monitoramento - Prometheus e Grafana](docs/prometheus-grafana.md)

#### - [Criar um usuário Deploy com permissão restrita](docs/restricted-user.md)

#### - [Kubernetes Dashboard](docs/dashboard.md)


## Exemplos:

#### - [Configurações](configurations)

#### - [Aplicações](applications)

#### - [CI/CD](cicd)